﻿using System;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.IO;
using System.Collections.Generic;

namespace HtmlToCsv
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i = 0; i <= 1000; i++)
            {
                procesaHtml();
            }

        }

        private void procesaHtml()
        {
            EmpresaModel empresa = new EmpresaModel();
            fileAdmin file = new fileAdmin();
            // Leo los archivos desde una carpeta
            // mapeo los datos del archivo
            // agregos los datos a un archivo sql
            // Muevo el archivo a otra carpeta procesado


            //

            string path = string.Empty;
            string nomArchivo = string.Empty;
            nomArchivo = file.nextFile(@"C:\htmltocsv\", "*.html");
            path = @"C:\htmltocsv\";

            var url = path + nomArchivo;


            var doc = new HtmlAgilityPack.HtmlDocument();
            try
            {
                doc.Load(url, System.Text.Encoding.UTF8);


                var nodes = doc.DocumentNode.SelectNodes("//tr");
                var nodesTable = doc.DocumentNode.SelectNodes("//table");
                string tableHtml = string.Empty;

                foreach (var node in nodes)
                {
                    string datoTh = string.Empty;
                    string datoTd = string.Empty;



                    if (node.ChildNodes.FindFirst("th") != null)
                    {
                        datoTh = node.ChildNodes.FindFirst("th").InnerText;
                        datoTh = datoTh.Replace("&iacute;", "i");
                        datoTh = datoTh.Trim();


                        if (datoTh == "Nombre de fantasia:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            datoTd = datoTd.Replace("&eacute;", "e");
                            empresa.nombreFantasia = datoTd;

                        }

                        if (datoTh == "Raz&oacute;n social:" || datoTh == "Razon social:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            datoTd = datoTd.Replace("&eacute;", "e");
                            empresa.razonSocial = datoTd;
                        }

                        if (datoTh == "Giro:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            datoTd = datoTd.Replace("&eacute;", "e");
                            empresa.giro = datoTd;

                        }

                        if (datoTh == "RUT:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            empresa.rut = datoTd;

                        }
                        if (datoTh == "Direcci&oacute;n:" || datoTh == "Direccion:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            empresa.direccion = datoTd;

                        }
                        if (datoTh == "Tel&eacute;fono:" || datoTh == "Telefono:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            empresa.telefono = datoTd;

                        }
                        if (datoTh == "Fax:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            empresa.fax = datoTd;

                        }
                        if (datoTh == "E-mail:")
                        {
                            datoTd = node.ChildNodes.FindFirst("td").InnerText;
                            datoTd = datoTd.Replace("&iacute;", "i");
                            empresa.email = datoTd.Trim();

                        }


                    }


                }//fin node tr

                foreach (var nodetable in nodesTable)

                    if (nodesTable.GetNodeIndex(nodetable) == 1)
                    {
                        var docTableContacto = new HtmlAgilityPack.HtmlDocument();
                        docTableContacto.LoadHtml(nodetable.InnerHtml);
                        var nodescontacto = docTableContacto.DocumentNode.SelectNodes("//td");

                        string[] arraycontacto = new string[4];

                        List<Array> contacto = new List<Array>();
                        int idContacto = 0;
                        int numeroParaElIf = 3;
                        foreach (var node in nodescontacto)
                        {
                            string datoTd = string.Empty;

                            datoTd = node.InnerText;
                            if (datoTd != "Cargo " && datoTd != "Nombre" && datoTd != "Fono" && datoTd != "E-mail")
                            {
                                //archivo += datoTd.Trim() + "\n";

                                arraycontacto[idContacto] = datoTd.Trim();

                                if (idContacto == numeroParaElIf)
                                {
                                    contacto.Add(arraycontacto);

                                    idContacto = -1;
                                    numeroParaElIf = 3;
                                    arraycontacto = new string[4];
                                }

                                idContacto++;
                            }
                        }

                        List<ContactoModel> contactoList = new List<ContactoModel>();

                        foreach (var contact in contacto)
                        {
                            ContactoModel contactoModel = new ContactoModel();
                            contactoModel.puesto = Convert.ToString(contact.GetValue(0));
                            contactoModel.nombre = Convert.ToString(contact.GetValue(1));
                            contactoModel.telefono = Convert.ToString(contact.GetValue(2));
                            contactoModel.email = Convert.ToString(contact.GetValue(3));
                            contactoModel.rutEmpresa = empresa.rut;
                            contactoModel.razonSocialEmpresa = empresa.razonSocial;

                            contactoList.Add(contactoModel);
                        }

                        empresa.contacto = contactoList;

                        empresa.save(empresa);

                    }
            }
            catch (Exception ex)
            {
             // MessageBox.Show("Error al leer archivo HTML: " + ex.Message);
            }

            file.mvFile(nomArchivo, path, @"C:\htmltocsv\procesado\");

        }
    }
}
