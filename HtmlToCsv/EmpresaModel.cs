﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlToCsv
{
    public class EmpresaModel
    {
        ConnectMysql con = new ConnectMysql();

        
        public string nombreFantasia { get; set; }
        public string razonSocial { get; set; }
        public string giro { get; set; }
        public string rut { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public List<ContactoModel> contacto { get; set; }

        public void save(EmpresaModel empresa)
        {

                OdbcConnection conexion = con.getConnect();
            try
            {
                OdbcCommand insert = new OdbcCommand();
                insert.Connection = conexion;
                insert.CommandText = "INSERT INTO empresas (nombreFantasia,razonSocial,giro,rut,direccion,telefono,fax,email) "
                                                            + "  VALUES ('"
                                                                   + empresa.nombreFantasia + "','"
                                                                   + empresa.razonSocial + "','"
                                                                   + empresa.giro + "','"
                                                                   + empresa.rut + "','"
                                                                   + empresa.direccion + "','"
                                                                   + empresa.telefono + "','"
                                                                   + empresa.fax + "','"
                                                                   + empresa.email + "'"
                                                                   + ");";
                OdbcDataReader reader = insert.ExecuteReader();
            }catch (Exception ex)
            {

            }

            finally
            {
                try
                {
                    conexion.Close();
                }
                catch (Exception ex)
                {

                }
            }

            foreach (var contact in empresa.contacto)
            {
                ContactoModel contacto = new ContactoModel();
                contacto.save(contact);
            }


        }

    }

    public class ContactoModel
    {
        ConnectMysql con = new ConnectMysql();
        public string rutEmpresa { get; set; }
        public string puesto { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public string razonSocialEmpresa { get; set; }



        public void save(ContactoModel contacto)
        {
            OdbcConnection conexion = con.getConnect();
            try
            {
                OdbcCommand insert = new OdbcCommand();
                insert.Connection = conexion;
                insert.CommandText = "INSERT INTO contactos (rutEmpresa,puesto,nombre,telefono,email,razonSocialEmpresa) "
                                                            + "  VALUES ('"
                                                                   + contacto.rutEmpresa + "','"
                                                                   + contacto.puesto + "','"
                                                                   + contacto.nombre + "','"
                                                                   + contacto.telefono + "','"
                                                                   + contacto.email + "','"
                                                                   + contacto.razonSocialEmpresa + "'"
                                                                   + ");";
                OdbcDataReader reader = insert.ExecuteReader();
            }
            finally
            {
                try
                {
                    conexion.Close();
                }
                catch (Exception ex)
                {

                }
            }

        }
    }


}
