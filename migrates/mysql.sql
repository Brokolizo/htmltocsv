CREATE TABLE empresas
( id INT(11) NOT NULL AUTO_INCREMENT,
  nombreFantasia VARCHAR(255),
  razonSocial VARCHAR(255),
  giro VARCHAR(255),
  rut VARCHAR(255),
  direccion VARCHAR(255),
  telefono VARCHAR(255),
  fax VARCHAR(255),
  email VARCHAR(255),
  CONSTRAINT empresas_pk PRIMARY KEY (id)
);

CREATE TABLE contactos
( id INT(11) NOT NULL AUTO_INCREMENT,
  rutEmpresa VARCHAR(255),
  puesto VARCHAR(255),
  nombre VARCHAR(255),
  telefono VARCHAR(255),
  email VARCHAR(255),
  CONSTRAINT contactos_pk PRIMARY KEY (id)
);


ALTER TABLE contactos ADD razonSocialEmpresa VARCHAR(255); 